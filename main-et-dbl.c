#include <stdio.h>

int dbl(int a);

int dbl (int a) {
    return a * 2;
}

int main() {
    printf("résultat: %i\n", dbl(4));
}