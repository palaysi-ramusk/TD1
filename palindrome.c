#include <stdbool.h>

int palindrome(char *s){
    int cpt = 0;
    bool est_palindrome = false;
    while(s[cpt] != '\0'){
        cpt++;
    }
    int i = 0;
    while (i < cpt / 2 && s[i] == s[cpt - i - 1]) {
        i++;
    }
    if (i == cpt / 2) {
        est_palindrome = true;
    }
    if (est_palindrome) {
        return 1;
    } else {
        return 0;
    }
}