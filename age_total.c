#include <stdio.h>

struct Personne {
    char *nom;
    int age;
    float taille;
};

struct Personne p;
char nom[] = "Guybrush";
p.nom = nom;
p.age = 22;
p.taille = 1.75;

printf("chaine: %lu, entier: %lu, flottant: %lu, personne: %lu\n",
    sizeof (char*),
    sizeof (int),
    sizeof (float),
    sizeof (struct Personne));



struct Famille {
    char *nom;
    struct Personne pere;
    struct Personne mere;
    int nb_enfants;
    struct Personne enfants[2];
};

struct Famille f;
f.nom = "Feur";
f.pere = p;
f.mere = p;
f.nb_enfants = 0;
f.enfants[0] = p;

printf("famille: %lu\n", sizeof(struct Famille));


int age_total(struct Famille f){
    int total = f.pere.age + f.mere.age;
    for(int i = 0; i < f.nb_enfants; i++ ){
        total += f.enfants[i].age;
    }
    return total;
}

struct Famille f2;
f2.nom = "Ratio";
f2.pere = p;
f2.mere = p;
f2.nb_enfants = 2;
f2.enfants[0] = p;
f2.enfants[1] = p;

printf("age total: %i\n", age_total(f2));

